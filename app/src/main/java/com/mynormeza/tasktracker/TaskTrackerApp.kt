package com.mynormeza.tasktracker

import android.app.Application
import io.realm.Realm
import io.realm.log.LogLevel
import io.realm.log.RealmLog
import io.realm.mongodb.App
import io.realm.mongodb.AppConfiguration

lateinit var taskApp: App
inline fun <reified T> T.TAG(): String = T::class.java.simpleName
class TaskTrackerApp : Application() {
    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
        taskApp = App(
            AppConfiguration.Builder(BuildConfig.MONGODB_REALM_APP_ID)
                .build())

        // Enable more logging in debug mode
        if (BuildConfig.DEBUG) {
            RealmLog.setLevel(LogLevel.ALL)
        }
    }
}